require "test_helper"

class AdventBoxControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get advent_box_index_url
    assert_response :success
  end
end

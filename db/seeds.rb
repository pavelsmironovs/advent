# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

AdventBox.create(
  description: "Tava pirmā dāvana meklējama vietā, kur kāds čalītis, kurš dzīvo šajā mājā, meklē savas trusenes",
  open_date: 2,
)

AdventBox.create(
  description: "Tava nākamā dāvana meklējama vietā, kuru tu sauc par 'cietumu'",
  open_date: 3,
)

AdventBox.create(
  description: "Dāvana ir zem vietas, kur atrodas galva, kad Katrīna čuč (ar atņemto sedziņu)",
  open_date: 8,
)

AdventBox.create(
  description: "Dāvana ir tur, kur parasti meklē dāvanas (treniņš)",
  open_date: 9,
)
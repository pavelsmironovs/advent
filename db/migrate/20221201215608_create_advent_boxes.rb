class CreateAdventBoxes < ActiveRecord::Migration[6.1]
  def change
    create_table :advent_boxes do |t|
      t.text :description
      t.boolean :is_active
      t.integer :open_date

      t.timestamps
    end
  end
end

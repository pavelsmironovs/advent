class AdventController < ApplicationController
  def index
    date = DateTime.now.day
    @object = AdventBox.where(open_date: date).first
  end
end
